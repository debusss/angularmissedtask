import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthenticationGuard } from './auth-guard/authentication.guard';
import { LoginComponent } from './login/login.component';


const routes: Routes = [{path:'',redirectTo:'login',pathMatch:'full'},{
  path:'login',component:LoginComponent
},
  { path: 'dashboard', loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),canActivate: [AuthenticationGuard] }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
