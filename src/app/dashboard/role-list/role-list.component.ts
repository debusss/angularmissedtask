import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CrudServiceService } from 'src/app/services/crud.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss']
})
export class RoleListComponent implements OnInit {
  title = 'angular-text-search-highlight';
  searchText = '';
  characters = [];
  userData: any;
  clickedUser: any;
  showForm:boolean;
    constructor(private _crudService: CrudServiceService,private router: Router) { }
    ngOnInit(): void {
      this.showForm=false;
 this.getUserDetails();
    }

   async getUserDetails(){
     let obj ={
      "list":  'RoleManagement',
      "connected": "false",
      "context":  1,
      "page": 1
    };
    let url = environment.base_url + environment.api_url.admin;
    let res =await this._crudService.globalAPICall(obj,'patch',url).toPromise();
    let body = JSON.parse(res.body);
    this.userData = body.output;
    this.setSearchData();
  }

    edit(user){
      this.showForm=true;
      this.clickedUser = {
        "id": user.id,
        "name": user.name,
        "description": user.description
      }
    }
    addItem(newItem: boolean) {
      this.showForm = newItem;
    }
    setSearchData(){
      for(let user of this.userData){
        this.characters.push(user.name);
      }
    }





}
