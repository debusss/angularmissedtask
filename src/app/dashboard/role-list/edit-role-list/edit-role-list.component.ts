import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudServiceService } from 'src/app/services/crud.service';
import { MessageService } from 'src/app/services/message.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-role-list',
  templateUrl: './edit-role-list.component.html',
  styleUrls: ['./edit-role-list.component.scss']
})
export class EditRoleListComponent implements OnInit {
  accessForm: FormGroup;
  @Input() item :any = [];
  @Output() boolForm = new EventEmitter<boolean>();

  constructor(private formBuilder: FormBuilder, private router: Router, private _crudService: CrudServiceService,private _notificationService : MessageService) {
   this.setValidators();
  }
  setValidators(){
    this.accessForm =this.formBuilder.group({
      id: new FormControl('', Validators.required),
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required)
    });

  }

  ngOnInit(): void {
    this.accessForm.setValue({
      "id":this.item.id,
      "name":this.item.name,
      "description":this.item.description
    });
  }

 async postProfile() {
    let obj = {
      "model": "RoleManagement",
      "value": {
        "id": this.accessForm.value.id
      },
      "changes": {
        "id":  this.accessForm.value.id,
        "name":  this.accessForm.value.name,
        "description":  this.accessForm.value.description
      },
      "context": 1
    };
    let url = environment.base_url + environment.api_url.admin;
    let res =await this._crudService.globalAPICall(obj,'post', url).toPromise();
    this.invisibleForm(false);
    window.location.reload();
  }

  invisibleForm(value: boolean) {
    this.boolForm.emit(value);
  }
}
