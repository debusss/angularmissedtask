import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthGuardService } from '../services/auth-guard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  show = false;
  constructor(private router: Router,private _authService : AuthGuardService) { }
  userName: string;
  name_of_user: string;

  ngOnInit(): void {
    this.userName=sessionStorage.getItem("userName");
    this.name_of_user = this.userName.slice(1,6);
  }
  logout(){
    this._authService.doLogout();
  }
}
