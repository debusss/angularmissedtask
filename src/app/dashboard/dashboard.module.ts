import { FilterPipe } from './../pipes/filter.pipe';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { UserListComponent } from './user-list/user-list.component';
import { RoleListComponent } from './role-list/role-list.component';
import { EditUserListComponent } from './user-list/edit-user-list/edit-user-list.component';
import { EditRoleListComponent } from './role-list/edit-role-list/edit-role-list.component';
import { HighlightDirective } from '../directive/highlight.directive';


@NgModule({
  declarations: [DashboardComponent,
    UserListComponent,
     RoleListComponent,
     EditUserListComponent,
      EditRoleListComponent,
      FilterPipe,
      HighlightDirective],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ]
})
export class DashboardModule { }
