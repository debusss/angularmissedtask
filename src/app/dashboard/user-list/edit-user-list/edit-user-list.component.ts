import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudServiceService } from 'src/app/services/crud.service';
import { MessageService } from 'src/app/services/message.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-user-list',
  templateUrl: './edit-user-list.component.html',
  styleUrls: ['./edit-user-list.component.scss']
})
export class EditUserListComponent implements OnInit {
  profileForm: FormGroup;
  @Input() item :any = [];
  @Output() boolForm = new EventEmitter<boolean>();
  error_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email length.' },
      { type: 'maxlength', message: 'Email length.' },
      { type: 'email', message: 'please enter a valid email address.' }
    ]
  }


  constructor(private formBuilder: FormBuilder, private router: Router, private _crudService: CrudServiceService,private _notificationService : MessageService) {
   this.setValidators();
  }
  setValidators(){
    this.profileForm =this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30),
        Validators.email
      ])),
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required)
    });

  }

  ngOnInit(): void {
    this.profileForm.setValue({
      "email":this.item.emailId,
      "firstname":this.item.firstname,
      "lastname":this.item.lastname
    });
  }

 async postProfile() {
    let obj = {
      "model": "UserManagement",
      "value": {
        "emailid": this.profileForm.value.email,
      },
      "changes": {
        "emailid":  this.profileForm.value.email,
        "firstname": this.profileForm.value.firstname,
        "lastname": this.profileForm.value.lastname
      },
      "context": 1
    };
    let url = environment.base_url + environment.api_url.admin;
    let res =await this._crudService.globalAPICall(obj,'post', url).toPromise();
    this.invisibleForm(false);
    window.location.reload();
  }

  invisibleForm(value: boolean) {
    this.boolForm.emit(value);
  }

}
