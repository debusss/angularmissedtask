import { FilterPipe } from './../../pipes/filter.pipe';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudServiceService } from 'src/app/services/crud.service';
import { MessageService } from 'src/app/services/message.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  title = 'angular-text-search-highlight';
  searchText = '';
  characters = [];
  userData: any = [];
  clickedUser: any;
  showForm:boolean=false;
    constructor(private _crudService: CrudServiceService,private router: Router) { }

    ngOnInit(): void {
      this.showForm=false;
  this.getUserDetails();

    }

   async getUserDetails(){
     let obj = {"list":"UserManagement",
     "connected":"false",
     "context":1,
     "page":1};
     let url = environment.base_url + environment.api_url.admin;
     let res =await this._crudService.globalAPICall(obj,'patch',url).toPromise();
     let body = JSON.parse(res.body);
     this.userData = body.output;
     this.setSearchData();
    }

    edit(user){
      this.showForm=true;
      this.clickedUser = {
        "emailId": user.emailid,
        "firstname": user.firstname,
        "lastname": user.lastname
      }
    }

  addItem(newItem: boolean) {
    this.showForm = newItem;
  }
  setSearchData(){
    for(let user of this.userData){
      this.characters.push(user.firstname);
    }
  }
}
