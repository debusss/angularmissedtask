import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { MessageService } from './../../services/message.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  notifcationText: string = '';
  showNotification: boolean = false;
  messages: any[] = [];
    subscription: Subscription;
  constructor(private notificationService: MessageService,
    private http: HttpClient,
    ) {
    // subscribe to home component messages
    this.subscription = this.notificationService.getMessage().subscribe(message => {
      if (message) {
        console.log(message);
        this.notifcationText = message.text;
        this.showNotification = message.boolval;
          setTimeout(()=>{
            this.showNotification = false;
          }, 3000)
      }
    });
}
  ngOnInit(): void {
  }


ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
}



}
