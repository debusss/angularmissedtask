import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {
  constructor(private router: Router) { }

  get isLoggedIn(): boolean {
    let authToken = sessionStorage.getItem('AccessToken');
    return (authToken !== null) ? true : false;
  }
  getAccessToken(){
    let accessToken = sessionStorage.getItem("AccessToken");
    return accessToken;
  }
  doLogout() {
    let removeToken = sessionStorage.removeItem('AccessToken');
    let removeToken1 = sessionStorage.removeItem("RefreshToken");
    let removeToken2 = sessionStorage.removeItem("IdToken");
    if (removeToken == null && removeToken1 == null && removeToken2 == null) {
      this.router.navigate(['login']);
    }
  }
}
