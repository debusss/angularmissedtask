import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CrudServiceService {

  constructor(private httpclient: HttpClient) {
  }

  globalAPICall(obj: any, method: string, url: string): Observable<any> {
    let httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': sessionStorage.getItem("IdToken")?sessionStorage.getItem("IdToken"):"",
      })
    };
    return this.httpclient[method]<any>(url, obj,httpOptions).pipe(
      map(res => {
        return res
      }),
      catchError((e: any) => {
        if (e.status === 401) {
        } else {
          return throwError(this.errorHandler(e))
        }
      })
    )
  }
  errorHandler(e) {
    console.log(e);
  }
}

