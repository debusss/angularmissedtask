import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { CrudServiceService } from '../services/crud.service';
import { EncrDecrService } from '../services/encr-decr.service';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  emailId = '';
  pword = '';

  error_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'minlength', message: 'Email length.' },
      { type: 'maxlength', message: 'Email length.' },
      { type: 'email', message: 'please enter a valid email address.' }
    ],

    'password': [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password length.' },
      { type: 'maxlength', message: 'password length.' }
    ]
  }

  constructor(private formBuilder: FormBuilder, private router: Router, private _globalService: CrudServiceService,
    private _Encr_Decr: EncrDecrService,
    private notificationService: MessageService) {
    this.loginForm = formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30),
        Validators.email
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(30)
      ]))

    })
  }


  ngOnInit(): void {
  }
  async Login() {
    if (this.loginForm.valid) {
      let Obj = {
        "clientId": environment.clientId,
        "userpool": environment.poolid,
        "username": this.loginForm.controls.email.value.toLowerCase(),
        "password": this._Encr_Decr.set(this.loginForm.controls.password.value)
      };
      let url = environment.base_url + environment.api_url.login;
      let method = 'post';
      let res = await this._globalService.globalAPICall(Obj, method, url).toPromise();
      let body = JSON.parse(res.body);
      let idToken = body.output.AuthenticationResult.IdToken;
      let accessToken = body.output.AuthenticationResult.AccessToken;
      let refreshToken = body.output.AuthenticationResult.RefreshToken;
      sessionStorage.setItem("IdToken",idToken);
      sessionStorage.setItem("AccessToken",accessToken);
      sessionStorage.setItem("RefreshToken",refreshToken);
      if (JSON.parse(res.statusCode) === 200) {
        alert("Login successsfully");
        this.notificationService.sendMessage('SuccessFully Login',true);
        setTimeout(() => {
          this.router.navigate(['dashboard'],{ skipLocationChange: true });
        }, 2000);
        sessionStorage.setItem("userName",JSON.stringify(Obj.username));
        sessionStorage.setItem('isLoggedIn', JSON.stringify(true));
      } else {
        this.router.navigate(['']);
        sessionStorage.setItem('isLoggedIn', JSON.stringify(false));
        alert("Login Failed");
      }
    }
  }

}
